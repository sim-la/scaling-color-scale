# 📏🌈 Scaling color scale

A tool to check the scaling of a color scale.

## 🎯 Purpose

This tool is useful to see how a color scale is scaled between two values.

To emphasize the differences between data points, and to make the data more readable, I found it useful to scale a color scale.
Here we are scaling a [turbo color scale](https://blog.research.google/2019/08/turbo-improved-rainbow-colormap-for.html) with a minimum and maximum value and a ratio.

The goal is to have a significant difference between the colors even if data points are close to each other.

## 📐 Equations

We have a minimum value $A$, a maximum value $B$, and a ratio $r$.
Given a local minimum value $x$ and a local maximum value $y$, we want the color scale to span from $a$ to $b$ such that $a$ is greater than $A$, $b$ is less than $B$, and the ratio between $a$ and $b$ is $r$.
Also we want the relative ratios to be preserved.

$$
\begin{cases}
  y - x = r \times (b - a) \\
  \frac{x - a}{x - A} = \frac{y - b}{y - B}
\end{cases}
$$

The solution is:
$$
\begin{cases}
  a = \frac{1}{B - A - (y - x)} \times \left[\frac{\left(1 - r\right) \times y - x}{r} \times \left(A - x\right) + \left(B - y\right) \times x\right] \\
  b = a + \frac{y - x}{r}
\end{cases}
$$

## 👀 Website

https://sim-la.gitlab.io/scaling-color-scale
